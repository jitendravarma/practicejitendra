class Employee:
	empcount=0
	def __init__(self, name, salary):
		self.name=name
		self.salary=salary
		Employee.empcount+=1

	def displayCount(self):
		print "This is Employee no: %d" %empcount

	def displayName(self):
		print "Name :",self.name,", Salary :", self.salary

Emp1=Employee("Allen",100000)
Emp2=Employee("Vladimir", 2000000)

Emp1.displayName()
Emp2.displayName()

print "Total Employees are %d" %Employee.empcount

print hasattr(Emp1, 'name')
print getattr(Emp1, 'name')
print setattr(Emp1, 'name','Bruce')


print "Lets print employee ka dict",Employee.__dict__
